#!/usr/bin/env python
# Author: Patrick Byrne
# Description:
#   Download a google sheet as a CSV file 

import requests
import sys

def get_sheet(file_id,fformat):
    """ Take a gdrive file id and an export file type. Return the contents. """
    # Build the URL
    url = 'https://docs.google.com/spreadsheets/d/' + file_id \
            + '/export?format=' + fformat
    # Try to retrieve the file and die if an exception is raised
    try:
        resp = requests.get(url)
    except:
        print('Error: Unable to retrieve sheet')
        sys.exit(1)
    # If the response code is OK, return the content. Otherwise die
    if resp.status_code == 200:
        return resp.content
    else:
        print('Error: Unexpected response when downloading sheet')
        sys.exit(1)

if __name__ == "__main__":
    """ Take basic arguments for function testing and standalone use. """
    if len(sys.argv) > 1:
        # If there are arguments, take the second as the file format
        if len(sys.argv) > 2:
            fformat = sys.argv[2]
        else:
            fformat = 'csv' # Default file format if there is only one argument
        # Write the output to a file called output.<file format>
        with open('output.' + fformat, 'wb') as f:
            f.write(get_sheet(sys.argv[1],fformat))
    else:
        # If there are no arguments, print the usage statement
        print('Usage: ' + sys.argv[0] + ' <Google File ID> [file format]')

